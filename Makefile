GO ?= go

VERSION ?= $(shell git describe --tags --always | sed 's/-/+/' | sed 's/^v//')

.PHONY: build
build:
	$(GO) build -ldflags '-s -w -X "go.jolheiser.com/vanity/router.Version=$(VERSION)"'

.PHONY: fmt
fmt: fmt-cli fmt-lib

.PHONY: test
test: test-cli test-lib

.PHONY: fmt-cli
fmt-cli:
	$(GO) fmt ./...

.PHONY: test-cli
test-cli:
	$(GO) test -race ./...

.PHONY: fmt-lib
fmt-lib:
	@cd go-vanity && $(GO) fmt ./...

.PHONY: test-lib
test-lib:
	@cd go-vanity && $(GO) test -race ./...

.PHONY: vet
vet:
	$(GO) vet ./...

.PHONY: docker-build
docker-build:
	docker build -f docker/Dockerfile -t jolheiser/vanity .

.PHONY: docker-push
docker-push:
	docker push jolheiser/vanity