# Vanity

A simple web service to serve [vanity Go imports](https://golang.org/cmd/go/#hdr-Remote_import_paths). Feel free to check it out using [my instance](https://go.jolheiser.com/).

Vanity also supports [git-import](https://gitea.com/jolheiser/git-import).

## Docker

```sh
docker run \
    --env VANITY_DOMAIN=go.domain.tld \
    --env VANITY_TOKEN=<token> \
    --publish 80:7777 \
    --restart always
    jolheiser/vanity:latest
```

## API

Check out the [SDK](go-vanity).

## License

[MIT](LICENSE)