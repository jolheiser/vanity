module go.jolheiser.com/vanity

go 1.16

replace go.jolheiser.com/vanity/go-vanity => ./go-vanity

require (
	github.com/AlecAivazis/survey/v2 v2.2.8
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/urfave/cli/v2 v2.2.0
	go.etcd.io/bbolt v1.3.5
	go.jolheiser.com/beaver v1.0.2
	go.jolheiser.com/overlay v0.0.2
	go.jolheiser.com/vanity/go-vanity v0.0.0-00010101000000-000000000000
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/sys v0.0.0-20200909081042-eff7692f9009 // indirect
	golang.org/x/text v0.3.3 // indirect
)
