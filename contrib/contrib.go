package contrib

import (
	_ "embed"
	"os"
	"strings"
)

//go:embed vanity.service
var SystemdService string

func init() {
	bin, err := os.Executable()
	if err != nil {
		return
	}
	SystemdService = os.Expand(SystemdService, func(s string) string {
		switch strings.ToUpper(s) {
		case "BIN":
			return bin
		case "VANITY_TOKEN":
			return os.Getenv("VANITY_TOKEN")
		case "VANITY_DOMAIN":
			return os.Getenv("VANITY_DOMAIN")
		}
		return ""
	})
}
