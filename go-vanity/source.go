package vanity

import (
	"errors"
	"fmt"
	"strings"
)

type SourceDirFile struct {
	Dir  string
	File string
}

func GiteaSDF(pkg Package) SourceDirFile {
	return SourceDirFile{
		Dir:  fmt.Sprintf("%s/src/branch/%s{/dir}", pkg.WebURL, pkg.Branch),
		File: fmt.Sprintf("%s/src/branch/%s{/dir}/{file}#L{line}", pkg.WebURL, pkg.Branch),
	}
}

func GitHubSDF(pkg Package) SourceDirFile {
	return SourceDirFile{
		Dir:  fmt.Sprintf("%s/tree/%s{/dir}", pkg.WebURL, pkg.Branch),
		File: fmt.Sprintf("%s/blob/%s{/dir}/{file}#L{line}", pkg.WebURL, pkg.Branch),
	}
}

func GitLabSDF(pkg Package) SourceDirFile {
	return SourceDirFile{
		Dir:  fmt.Sprintf("%s/-/tree/%s{/dir}", pkg.WebURL, pkg.Branch),
		File: fmt.Sprintf("%s/-/blob/%s{/dir}/{file}#L{line}", pkg.WebURL, pkg.Branch),
	}
}

func AnalyzeSDF(pkg Package) (SourceDirFile, error) {
	switch {
	case strings.Contains(pkg.WebURL, "gitea.com"):
		return GiteaSDF(pkg), nil
	case strings.Contains(pkg.WebURL, "github.com"):
		return GitHubSDF(pkg), nil
	case strings.Contains(pkg.WebURL, "gitlab.com"):
		return GitLabSDF(pkg), nil
	}
	return SourceDirFile{}, errors.New("could not detect SDF")
}
