package vanity

import (
	"context"
	"io"
	"net/http"
)

const (
	DefaultServer = "https://go.jolheiser.com"
	TokenHeader   = "X-Vanity-Token"
)

type Client struct {
	token  string
	server string
	http   *http.Client
}

func New(token string, opts ...ClientOption) *Client {
	c := &Client{
		token:  token,
		server: DefaultServer,
		http:   http.DefaultClient,
	}
	for _, opt := range opts {
		opt(c)
	}
	return c
}

type ClientOption func(*Client)

func WithHTTP(client *http.Client) ClientOption {
	return func(c *Client) {
		c.http = client
	}
}

func WithServer(server string) ClientOption {
	return func(c *Client) {
		c.server = server
	}
}

func (c *Client) newRequest(ctx context.Context, method, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set(TokenHeader, c.token)
	return req, nil
}
