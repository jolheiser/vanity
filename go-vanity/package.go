package vanity

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

type Info struct {
	Version     string    `json:"version"`
	NumPackages int       `json:"num_packages"`
	Packages    []Package `json:"packages"`
}

type Package struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Branch      string `json:"branch"`
	WebURL      string `json:"web_url"`
	CloneHTTP   string `json:"clone_http"`
	CloneSSH    string `json:"clone_ssh"`
}

func (p Package) Module(domain string) string {
	return fmt.Sprintf("%s/%s", strings.TrimSuffix(domain, "/"), strings.ToLower(p.Name))
}

// Info gets Info from a vanity server
func (c *Client) Info(ctx context.Context) (Info, error) {
	var info Info
	resp, err := c.crud(ctx, Package{}, http.MethodOptions)
	if err != nil {
		return info, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return info, fmt.Errorf("could not get info: %s", resp.Status)
	}

	if err := json.NewDecoder(resp.Body).Decode(&info); err != nil {
		return info, err
	}

	return info, nil
}

// Add adds a new Package to a vanity server
func (c *Client) Add(ctx context.Context, pkg Package) error {
	resp, err := c.crud(ctx, pkg, http.MethodPost)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("could not add package: %s", resp.Status)
	}
	return nil
}

// Update updates a Package on a vanity server
func (c *Client) Update(ctx context.Context, pkg Package) error {
	resp, err := c.crud(ctx, pkg, http.MethodPatch)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("could not update package: %s", resp.Status)
	}

	return nil
}

// Remove removes a Package from a vanity server
func (c *Client) Remove(ctx context.Context, pkg Package) error {
	resp, err := c.crud(ctx, pkg, http.MethodDelete)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("could not remove package: %s", resp.Status)
	}

	return nil
}

func (c *Client) crud(ctx context.Context, pkg Package, method string) (*http.Response, error) {
	payload, err := json.Marshal(pkg)
	if err != nil {
		return nil, err
	}

	req, err := c.newRequest(ctx, method, c.server, bytes.NewReader(payload))
	if err != nil {
		return nil, err
	}

	return c.http.Do(req)
}
