package cmd

import (
	"context"

	"go.jolheiser.com/vanity/cmd/flags"
	"go.jolheiser.com/vanity/database"
	"go.jolheiser.com/vanity/go-vanity"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Remove = cli.Command{
	Name:    "remove",
	Aliases: []string{"rm"},
	Usage:   "Remove package(s)",
	Before:  localOrToken,
	Action:  doRemove,
}

func doRemove(_ *cli.Context) error {
	pkgs, err := listPackages()
	if err != nil {
		return err
	}

	pkgSlice := make([]string, len(pkgs))
	pkgMap := make(map[string]vanity.Package)
	for idx, pkg := range pkgs {
		pkgSlice[idx] = pkg.Name
		pkgMap[pkg.Name] = pkg
	}

	pkgQuestion := &survey.Select{
		Message: "Select package to remove",
		Options: pkgSlice,
	}

	var pkgName string
	if err := survey.AskOne(pkgQuestion, &pkgName); err != nil {
		return err
	}

	pkg := vanity.Package{
		Name: pkgName,
	}

	if flags.Local {
		db, err := database.Load(flags.Database)
		if err != nil {
			return err
		}
		if err := db.RemovePackage(pkg.Name); err != nil {
			return err
		}
	} else {
		client := vanity.New(flags.Token, vanity.WithServer(flags.Server))
		if err := client.Remove(context.Background(), pkg); err != nil {
			return err
		}
	}

	beaver.Infof("Removed %s", yellow.Format(pkgName))
	return nil
}
