package cmd

import (
	"errors"
	"fmt"
	"net/http"

	"go.jolheiser.com/vanity/cmd/flags"
	"go.jolheiser.com/vanity/database"
	"go.jolheiser.com/vanity/router"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Server = cli.Command{
	Name:    "server",
	Aliases: []string{"web"},
	Usage:   "Start the vanity server",
	Flags: []cli.Flag{
		&cli.IntFlag{
			Name:        "port",
			Aliases:     []string{"p"},
			Usage:       "Port to run the vanity server on",
			Value:       3333,
			EnvVars:     []string{"VANITY_PORT"},
			Destination: &flags.Port,
		},
		&cli.StringFlag{
			Name:        "domain",
			Aliases:     []string{"d"},
			Usage:       "The Go module domain (e.g. go.jolheiser.com)",
			EnvVars:     []string{"VANITY_DOMAIN"},
			Destination: &flags.Domain,
		},
	},
	Action: doServer,
}

func doServer(_ *cli.Context) error {
	if flags.Token == "" || flags.Domain == "" {
		return errors.New("vanity server requires --token and --domain")
	}

	db, err := database.Load(flags.Database)
	if err != nil {
		beaver.Fatalf("could not load database at %s: %v", flags.Database, err)
	}

	beaver.Infof("Running vanity server at http://localhost:%d", flags.Port)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", flags.Port), router.New(flags.Token, db)); err != nil {
		return err
	}
	return nil
}
