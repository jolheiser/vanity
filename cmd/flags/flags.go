package flags

var (
	Server   string
	Domain   string
	Token    string
	Database string

	Local bool
	Force bool
	Port  int

	SystemdService bool
)
