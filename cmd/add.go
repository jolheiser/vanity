package cmd

import (
	"context"
	"net/url"

	"go.jolheiser.com/vanity/cmd/flags"
	"go.jolheiser.com/vanity/database"
	"go.jolheiser.com/vanity/go-vanity"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Add = cli.Command{
	Name:    "add",
	Aliases: []string{"a"},
	Usage:   "Add a package",
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:        "force",
			Aliases:     []string{"f"},
			Usage:       "Overwrite existing package without prompt",
			Destination: &flags.Force,
		},
		&cli.BoolFlag{
			Name:        "local",
			Aliases:     []string{"l"},
			Usage:       "local mode",
			Destination: &flags.Local,
		},
	},
	Before: localOrToken,
	Action: doAdd,
}

func doAdd(_ *cli.Context) error {
	pkg, err := pkgPrompt(vanity.Package{})
	if err != nil {
		return err
	}

	if flags.Local {
		db, err := database.Load(flags.Database)
		if err != nil {
			return err
		}
		if err := db.PutPackage(pkg); err != nil {
			return err
		}
	} else {
		client := vanity.New(flags.Token, vanity.WithServer(flags.Server))
		if err := client.Add(context.Background(), pkg); err != nil {
			return err
		}
	}

	beaver.Infof("Added %s", yellow.Format(pkg.Name))
	return nil
}

func validURL(ans interface{}) error {
	if err := survey.Required(ans); err != nil {
		return err
	}
	_, err := url.Parse(ans.(string))
	return err
}
