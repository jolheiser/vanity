package cmd

import (
	"context"

	"go.jolheiser.com/vanity/cmd/flags"
	"go.jolheiser.com/vanity/database"
	"go.jolheiser.com/vanity/go-vanity"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Update = cli.Command{
	Name:    "update",
	Aliases: []string{"u"},
	Usage:   "Update a package",
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:        "local",
			Aliases:     []string{"l"},
			Usage:       "local mode",
			Destination: &flags.Local,
		},
	},
	Before: localOrToken,
	Action: doUpdate,
}

func doUpdate(_ *cli.Context) error {
	pkgs, err := listPackages()
	if err != nil {
		return err
	}

	pkgSlice := make([]string, len(pkgs))
	pkgMap := make(map[string]vanity.Package)
	for idx, pkg := range pkgs {
		pkgSlice[idx] = pkg.Name
		pkgMap[pkg.Name] = pkg
	}

	pkgQuestion := &survey.Select{
		Message: "Select package to update",
		Options: pkgSlice,
	}

	var pkgName string
	if err := survey.AskOne(pkgQuestion, &pkgName); err != nil {
		return err
	}

	pkg, err := pkgPrompt(pkgMap[pkgName])
	if err != nil {
		return err
	}

	if flags.Local {
		db, err := database.Load(flags.Database)
		if err != nil {
			return err
		}
		if err := db.PutPackage(pkg); err != nil {
			return err
		}
	} else {
		client := vanity.New(flags.Token, vanity.WithServer(flags.Server))
		if err := client.Update(context.Background(), pkg); err != nil {
			return err
		}
	}

	beaver.Infof("Updated %s", yellow.Format(pkgName))
	return nil
}
