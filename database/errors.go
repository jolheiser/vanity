package database

import "fmt"

type ErrPackageNotFound struct {
	Name string
}

func (e ErrPackageNotFound) Error() string {
	return fmt.Sprintf("package not found: %s", e.Name)
}

func IsErrPackageNotFound(err error) bool {
	_, ok := err.(ErrPackageNotFound)
	return ok
}
