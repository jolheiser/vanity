package router

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"go.jolheiser.com/vanity/database"
	"go.jolheiser.com/vanity/go-vanity"

	"go.jolheiser.com/beaver"
)

var (
	server *httptest.Server
	token  = "TestingRouter"
)

// NOTE: The router test is more or less a copy/paste from go-vanity
// However, this ensures that testing is the same with the "real" router and DB
func TestMain(m *testing.M) {
	tmp, err := os.MkdirTemp(os.TempDir(), "vanity")
	if err != nil {
		panic(err)
	}
	dbPath := filepath.Join(tmp, "vanity.db")

	db, err := database.Load(dbPath)
	if err != nil {
		beaver.Fatalf("could not load database at %s: %v", dbPath, err)
	}

	server = httptest.NewServer(New(token, db))

	code := m.Run()

	// Cleanup
	if err := os.RemoveAll(tmp); err != nil {
		panic(err)
	}

	os.Exit(code)
}

func TestRouter(t *testing.T) {
	ctx := context.Background()
	client := vanity.New("", vanity.WithServer(server.URL))

	// Info
	checkInfo(t, client, 0)

	pkg1 := vanity.Package{
		Name:        "test1",
		Description: "test1",
		Branch:      "main",
		WebURL:      "https://gitea.com/jolheiser/test1",
		CloneHTTP:   "https://gitea.com/jolheiser/test1.git",
		CloneSSH:    "https://gitea.com/jolheiser/test1",
	}
	pkg2 := vanity.Package{
		Name:        "test2",
		Description: "test2",
		Branch:      "main",
		WebURL:      "https://gitea.com/jolheiser/test2",
		CloneHTTP:   "https://gitea.com/jolheiser/test2.git",
		CloneSSH:    "https://gitea.com/jolheiser/test2",
	}

	// Add (without token)
	if err := client.Add(ctx, pkg1); err == nil {
		t.Log("adding without token should fail")
		t.Fail()
	}

	// Add (with token)
	client = vanity.New(token, vanity.WithServer(server.URL))
	checkAdd(t, client, pkg1, pkg2)

	// Info (after second package)
	checkInfo(t, client, 2)

	// Check invalid package (404)
	checkResp(t, "test3", http.StatusNotFound)

	// Check valid package (200)
	checkResp(t, "test1", http.StatusOK)

	// Check valid sub-package (200)
	checkResp(t, "test1/foo/bar", http.StatusOK)

	// Update package
	checkUpdate(t, client, pkg1)

	// Remove
	checkRemove(t, client, pkg1)

	// Info (final)
	checkInfo(t, client, 1)
}

func checkInfo(t *testing.T, client *vanity.Client, numPackages int) {
	info, err := client.Info(context.Background())
	if err != nil {
		t.Logf("info should not return error: %v\n", err)
		t.Fail()
	}
	if info.Version != Version || info.NumPackages != numPackages {
		t.Log("info did not match expected")
		t.Fail()
	}
}

func checkAdd(t *testing.T, client *vanity.Client, pkg1, pkg2 vanity.Package) {
	ctx := context.Background()
	if err := client.Add(ctx, pkg1); err != nil {
		t.Logf("pkg1 should be added: %v\n", err)
		t.Fail()
	}
	if err := client.Add(ctx, pkg2); err != nil {
		t.Logf("pkg2 should be added: %v\n", err)
		t.Fail()
	}
	// Duplicate package
	if err := client.Add(ctx, pkg1); err == nil {
		t.Log("pkg1 should already exist")
		t.Fail()
	}
}

func checkUpdate(t *testing.T, client *vanity.Client, pkg vanity.Package) {
	ctx := context.Background()
	// Update invalid package
	if err := client.Update(ctx, vanity.Package{Name: "test4"}); err == nil {
		t.Log("should not be able to update invalid package")
		t.Fail()
	}

	// Update valid package
	if err := client.Update(ctx, pkg); err != nil {
		t.Logf("should be able to update valid package: %v\n", err)
		t.Fail()
	}
}

func checkRemove(t *testing.T, client *vanity.Client, pkg vanity.Package) {
	ctx := context.Background()
	if err := client.Remove(ctx, pkg); err != nil {
		t.Logf("should be able to remove package: %v\n", err)
		t.Fail()
	}

	// Remove (idempotent)
	if err := client.Remove(ctx, pkg); err != nil {
		t.Logf("should be able to remove package idempotently: %v\n", err)
		t.Fail()
	}
}

func checkResp(t *testing.T, path string, status int) {
	resp, err := http.Get(fmt.Sprintf("%s/%s", server.URL, path))
	if err != nil {
		t.Logf("could not GET %s: %v", path, err)
		t.Fail()
		return
	}

	if resp.StatusCode != status {
		t.Logf("incorrect response from %s, expected %d but got %d", path, status, resp.StatusCode)
		t.Fail()
	}
}
