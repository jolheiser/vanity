package router

import (
	"embed"
	"html/template"
	"os"
	"path/filepath"
	"runtime"

	"go.jolheiser.com/overlay"
)

var (
	//go:embed templates
	templateFS embed.FS
	Version    string
)

func tmpl(name string) (*template.Template, error) {
	bin, err := os.Executable()
	if err != nil {
		return nil, err
	}
	customPath := os.Getenv("VANITY_CUSTOM")
	if customPath == "" {
		customPath = filepath.Join(bin, "custom")
	}

	ofs, err := overlay.New(customPath, templateFS)
	if err != nil {
		return nil, err
	}

	return template.New(name).Funcs(funcMap).ParseFS(ofs, "templates/base.tmpl", "templates/"+name)
}

var funcMap = template.FuncMap{
	"AppVer": func() string {
		return Version
	},
	"GoVer": func() string {
		return runtime.Version()
	},
}
