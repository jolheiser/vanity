package main

import (
	"os"

	"go.jolheiser.com/vanity/cmd"

	"go.jolheiser.com/beaver"
)

func main() {

	beaver.Console.Format = beaver.FormatOptions{
		TimePrefix:  true,
		StackPrefix: true,
		StackLimit:  15,
		LevelPrefix: true,
		LevelColor:  true,
	}

	if err := cmd.New().Run(os.Args); err != nil {
		beaver.Fatal(err)
	}
}
